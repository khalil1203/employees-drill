/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

// const data = {
//     "employees": [
//         {
//             "id": 23,
//             "name": "Daphny",
//             "company": "Scooby Doo"
//         },
//         {
//             "id": 73,
//             "name": "Buttercup",
//             "company": "Powerpuff Brigade"
//         },
//         {
//             "id": 93,
//             "name": "Blossom",
//             "company": "Powerpuff Brigade"
//         },
//         {
//             "id": 13,
//             "name": "Fred",
//             "company": "Scooby Doo"
//         },
//         {
//             "id": 89,
//             "name": "Welma",
//             "company": "Scooby Doo"
//         },
//         {
//             "id": 92,
//             "name": "Charles Xavier",
//             "company": "X-Men"
//         },
//         {
//             "id": 94,
//             "name": "Bubbles",
//             "company": "Powerpuff Brigade"
//         },
//         {
//             "id": 2,
//             "name": "Xyclops",
//             "company": "X-Men"
//         }
//     ]
// }

const fs = require('fs');



const writeFile = (path, data) => {
    fs.writeFile(path, data,(err)=>{
        if(err){
            return;
        }
    });
};




// Question 1 : Retrieve data for ids : [2, 13, 23].


function answer1() {
    fs.readFile('./data.json', 'utf-8', (err, data) => {
        if (err) {
            return;
        }
        else {
            const jsonObject = JSON.parse(data);
            const ques_arr = [2, 13, 23];
            const answer1_arr = Object.values(jsonObject)[0].filter((entries) => {
                if (ques_arr.includes(entries.id)) {
                    return true;
                }
                else {
                    return false;
                }
            });
            writeFile('./answer1.json', JSON.stringify(answer1_arr));
            console.log(answer1_arr);
            answer2();
        }
    });
    
}






// Question 2 : Group data based on companies.

function answer2() {
    fs.readFile('./data.json', 'utf-8', (err, data) => {
        if (err) {
            return;
        }
        else {
            const jsonObject = JSON.parse(data);
            const answer2_arr = Object.values(jsonObject)[0].reduce((acc, entries) => {
                if (acc[entries.company]) {
                    acc[entries.company].push(entries);
                }
                else {
                    acc[entries.company] = [entries];
                }
                return acc;
            }, {});
            writeFile('./answer2.json', JSON.stringify(answer2_arr));
            console.log(answer2_arr);
            answer3();
        }
    });

}




// Question 3 :  Get all data for company Powerpuff Brigade


function answer3() {
    fs.readFile('./data.json', 'utf-8', (err, data) => {
        if (err) {
            return;
        }
        else {
            const jsonObject = JSON.parse(data);
            const answer3_arr = Object.values(jsonObject)[0].filter((entries) => {
                if ((entries.company == "Powerpuff Brigade")) {
                    return true;
                }
                else {
                    return false;
                }
            });
            writeFile('./answer3.json', JSON.stringify(answer3_arr));
            console.log(answer3_arr);
            answer4();
        }
    });
}





// Question 4: Remove entry with id 2.


function answer4() {
    fs.readFile('./data.json', 'utf-8', (err, data) => {
        if (err) {
            return;
        }
        else {
            const jsonObject = JSON.parse(data);
            const answer4_arr = Object.values(jsonObject)[0].map((entries) => {
                if(!(entries.id == 2)){
                    return {...entries};
                }
            }, {});
            writeFile('./answer4.json', JSON.stringify(answer4_arr));
            console.log(answer4_arr);
        }
    });

}
answer1();